Feature: Basic logic for login and registration
  In order to

  #Scenario: Successful login
    #Given there is a user with valid email
    #And the user is making a registration with password Qwemnb147896325
    #When the user presses button Register
    #Then the user is successfully registered
    #
Scenario Outline: Successful login
    Given there is a user with valid email
    And the user is making a registration with Password <password>
    When the user presses button Register
    Then the user is successfully registered
    
    Examples:
    
      | password          |
      |Qwemnb14789632525346747557u6    |
      |dfwgg              |
    
    
    #Given a user with username 'admin' and password 'adm123' is logged
#
  #Scenario Outline: Successful login
    #Given there is a user with username <Username> and password <Password>
    #When the user is logged in the system
    #Then the user is redirected to the home page
#
    #Examples:
      #| Username | Password              |
      #| admin    | admin123              |
      #| dev      | dev123$               |
      #| qa       | ^*&%8965gjhgjhgYGyjkg |
#
  #Scenario: Complex user
    #Given there is a user with the following data:
      #| FirstName | FamilyName | Address      | Occupation | Age |
      #| Ivan      | Dimitrov   | Lozenetz 123 | Analist    | 42  |

