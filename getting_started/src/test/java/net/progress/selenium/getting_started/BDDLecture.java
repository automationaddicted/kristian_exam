package net.progress.selenium.getting_started;

import contexts.Context;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import io.cucumber.datatable.DataTable;
import pageObjects.LoginPageModel;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;

public class BDDLecture {
    private LoginPageModel loginPage;
    private Context context;

    public BDDLecture(Context context){
        this.context=context;
        this.loginPage = new LoginPageModel();
    }

    @Given("there is a user with valid email")
    public void logIn(){
        loginPage.navigateToPage();
    }
    
    @Given("there is a user without register email")
    public void theuserIsTryToregisterWithoutEmail() {
    }

    @Given("the user is making a registration with Password (.*)")
    public void theUserIsMakingARegistration(String password) {
        context.password=password;
    }
    
    

    @When("the user presses button Register")
    public void theUserPressesButtonRegister() {
        loginPage.register(context.password);
    }

    @Then("the user is successfully registered")
    public void theUserIsSuccessfullyRegistered() {
        assertTrue("Login is not successful",
                loginPage.isRedirectSuccessful());
    }

    @Given("a user with username {string} and password {string} is logged")
    public void loginWithParameters(String username, String pass){

    }

    @Given("there is a user with username <Username> and password <Password>")
    public void thereIsAUserWithUsernameUsernameAndPasswordPassword() {
        
    }

    @When("the user is logged in the system")
    public void theUserIsLoggedInTheSystem() {
        
    }

    @Then("the user is redirected to the home page")
    public void theUserIsRedirectedToTheHomePage() {

    }

    @Given("there is a user with the following data:")
    public void thereIsAUserWithTheFollowingData(DataTable dataTable) {
        List<Map<String, String>> userData= dataTable.asMaps(String.class, String.class);
    }
}
