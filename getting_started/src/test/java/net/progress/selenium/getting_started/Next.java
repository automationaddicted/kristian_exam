package net.progress.selenium.getting_started;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Common;

import java.util.List;
import java.util.function.Function;

public class Next {
    @Test
    public void selectMenuItem(){
        WebDriver driver = Common.getDriver();
        driver.get("https://www.toolsqa.com/category/blogs/");

        WebDriverWait wait = new WebDriverWait(driver, 10);

        WebElement mainItem=driver.findElement(By.cssSelector("#primary-menu > li:nth-child(3) > a > span > span"));

        Actions action=new Actions(driver);
        action.moveToElement(mainItem).build().perform();
        WebElement subItem1=wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#primary-menu > li:nth-child(3) > ul > li:nth-child(1) > a > span > span")));
        action.moveToElement(subItem1).build().perform();
        WebElement subItem2=wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#primary-menu > li:nth-child(3) > ul > li:nth-child(1) > ul > li:nth-child(1) > a > span > span")));
        action.moveToElement(subItem2).click().build().perform();
    }

    @Test
    public void registrationForm(){
        WebDriver driver = Common.getDriver();
        driver.get("http://demo.automationtesting.in/Register.html");

        Select language=new Select(driver.findElement(By.id("Skills")));
        language.getOptions();
    }


    @Test
    public void testFluent(){
        WebDriver driver=Common.getDriver();
        driver.get("https://www.toolsqa.com/category/blogs/");

        WebDriverWait wait=new WebDriverWait(driver, 10);

        WebElement webElement= wait.until(new Function<WebDriver, WebElement>(){
            public WebElement apply(WebDriver driver ) {
                String cssSelector="#primary-menu > li.menu-item.menu-item-type-custom.menu-item-object-custom.current-" +
                        "menu-item.menu-item-34232.act > a > span > span";
                List<WebElement> list=driver.findElements(By.cssSelector(cssSelector));
                for(int i=0;i<60;i++){
                    if(list.size()>0){
                        break;
                    }
                    try {
                        Thread.sleep(500);
                        list=driver.findElements(By.cssSelector(cssSelector));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                if(list.size()>0){
                    return list.get(0);
                }else{
                    return null;
                }
            }
        });



    }
}
