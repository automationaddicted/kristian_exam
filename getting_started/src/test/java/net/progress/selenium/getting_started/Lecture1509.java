package net.progress.selenium.getting_started;

import junit.framework.Assert;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Common;

import java.util.Set;

public class Lecture1509 {
    @Test
    public void switchWindows(){
        WebDriver driver=Common.getDriver();
        driver.get("https://www.toolsqa.com/automation-practice-switch-windows/");

        driver.findElement(By.cssSelector("#content > div.vc_row.wpb_row.vc_row-fluid.dt-default > " +
                "div.wpb_column.vc_column_container.vc_col-sm-8 > div > div > div > div > " +
                "p:nth-child(6) > button")).click();

        //Switching to the message window
        String currentHandle=driver.getWindowHandle();
        switchToNewWindow(driver, currentHandle);

        Assert.assertEquals("Text in message window is not the expected one",
                "Knowledge increases by sharing but not by saving. Please share this website with " +
                        "your friends and in your organization.",
                driver.findElement(By.cssSelector("body")).getText());
        driver.close();
        driver.switchTo().window(currentHandle);

        int openWindowsCountBefore=driver.getWindowHandles().size();
        driver.findElement(By.id("button1")).click();
        int openWindowsCountAfter=driver.getWindowHandles().size();
        Assert.assertEquals("Windows count after opening new browser window is not the expected one", openWindowsCountBefore+1,openWindowsCountAfter);

        switchToNewWindow(driver, currentHandle);
        Assert.assertEquals("New window URL is not the expected one", "https://www.toolsqa.com/", driver.getCurrentUrl());
        driver.quit();
    }

    @Test
    public void waitPractice() throws InterruptedException {
        WebDriver driver = Common.getDriver();
        driver.get("https://www.toolsqa.com/automation-practice-switch-windows/");

        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,400)");
        Thread.sleep(500);

        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement button = wait.until(ExpectedConditions.elementToBeClickable(By.id("timingAlert")));
        button.click();

        Alert alert = wait.until(ExpectedConditions.alertIsPresent());
        Assert.assertNotNull("The alert is not displayed during the given timeout", alert);
        driver.quit();
    }

    private void switchToNewWindow(WebDriver driver, String currentHandle){
        Set<String> handles=driver.getWindowHandles();
        for(String handle:handles){
            if(!handle.equals(currentHandle)){
                driver.switchTo().window(handle);
                break;
            }
        }
    }
}


