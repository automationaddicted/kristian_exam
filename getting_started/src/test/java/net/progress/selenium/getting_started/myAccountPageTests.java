package net.progress.selenium.getting_started;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import junit.framework.Assert;
import junit.framework.TestCase;
import utils.Common;

public class myAccountPageTests extends TestCase{
	public void registerWithInvalidEmail() {
		WebDriver driver=Common.getDriver();
		driver.get("http://practice.automationtesting.in/my-account/");
		driver.findElement(By.id("reg_email")).sendKeys("registerWithInvalidEmail@as");
		driver.findElement(By.id("reg_password")).sendKeys("MyPassword12!@#$*");
		driver.findElement(By.cssSelector("#customer_login > div.u-column2.col-2 > "
				+ "form > p.woocomerce-FormRow.form-row > "
				+ "input.woocommerce-Button.button")).click();
		
		String errorMessageText = driver.findElement(By.className("woocommerce-error")).getText();
		System.out.println(errorMessageText);
		assertEquals(errorMessageText,"Error: Please provide a valid email address.");
	}
	
	
	public void logInWithIncorrectUsernameAndPassword() throws InterruptedException {
		WebDriver driver=Common.getDriver();
		driver.get("http://practice.automationtesting.in/my-account/");
		driver.findElement(By.id("username")).sendKeys("notValidUsername");
		driver.findElement(By.id("password")).sendKeys("wrongPass");
		driver.findElement(By.cssSelector("#customer_login > div.u-column1.col-1 > "
				+ "form > p:nth-child(3) > input.woocommerce-Button.button")).click();

////		String notValidUsername=": Invalid username. ";
////		Assert.assertEquals(String.format("There is no element with CSS selector %s", notValidUsername), 
////				1, driver.findElements(By.cssSelector(notValidUsername)).size());
////		Assert.assertEquals(": Invalid username. ", notValidUsername,
////				driver.findElement(By.cssSelector(notValidUsername)).getText());
////		
		
		String errorMessageText = driver.findElement(By.className("woocommerce-error")).getText();
		System.out.println(errorMessageText);
		assertEquals(errorMessageText,"ERROR: Invalid username. Lost your password?");
}
}