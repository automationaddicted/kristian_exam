package net.progress.selenium.getting_started;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import pageObjects.ShopExam;
import utils.Common;

public class ShopExamPF {
	ShopExam shopexam;
	WebDriver driver;

	public ShopExamPF() {
		driver = Common.getDriver();
		shopexam = new ShopExam(driver);
	}

	public void loadShopPage() {
		shopexam.load();
		shopexam.isLoaded();
	}

	@Test
	public void pressSighinbutton() throws InterruptedException {
		loadShopPage();
		shopexam.pressSignInButton();

		assertEquals("Redirect is not successful", "CREATE AN ACCOUNT",
				shopexam.isRedirectSuccessfulToCreateanAccount());

		driver.quit();

	}

	@Test
	public void sendEmailAdress() throws InterruptedException {
		loadShopPage();
		shopexam.pressSignInButton();
		shopexam.sendEmail();

		assertEquals("Redirect is not successful", "YOUR PERSONAL INFORMATION",
				shopexam.isRedirectSuccessfulToPersonalInfo());

		driver.quit();

	}

	@Test
	public void sendInvalidEmail() throws InterruptedException {
		loadShopPage();
		shopexam.pressSignInButton();
		shopexam.sendInvalidEmail();

		String errormessageemail = shopexam.checkErrorMessageEmail();
		assertEquals("You send invalid email address", "Invalid email address.", errormessageemail);

		driver.quit();
	}

	@Test
	public void createNewaccountonlyMandatoryFields() throws InterruptedException {
		loadShopPage();
		shopexam.pressSignInButton();
		shopexam.sendEmail();
		shopexam.createNewaccountonlyMandatoryFields("Kristian", "Dilovski", "Adfweg1234", "Sofia,Bulgaria", "Sofia",
				"18035", "0893426543", "SF");

		String myaccountcheck = shopexam.checkMyaccountPagetitle();
		assertEquals("You are not on the My account page ", "MY ACCOUNT", myaccountcheck);

		driver.quit();

	}

	@Test
	public void createAccountandSignout() throws InterruptedException {
		loadShopPage();
		shopexam.pressSignInButton();
		shopexam.sendEmail();
		shopexam.createNewaccountonlyMandatoryFields("Kristian", "Dilovski", "Adfweg1234", "Sofia,Bulgaria", "Sofia",
				"18035", "0893426543", "SF");
		shopexam.pressSignoutButton();

		assertEquals("Redirect is not successful", "CREATE AN ACCOUNT",
				shopexam.isRedirectSuccessfulToCreateanAccount());

		driver.quit();

	}

	@Test
	public void createNewaccountandLogin() throws InterruptedException {
		loadShopPage();
		shopexam.pressSignInButton();
		shopexam.loginPositive();

		String myaccountcheck = shopexam.checkMyaccountPagetitle();
		assertEquals("You are not on the My account page ", "MY ACCOUNT", myaccountcheck);

		driver.quit();
	}

	@Test
	public void loginWithWrongEmail() throws InterruptedException {
		loadShopPage();
		shopexam.pressSignInButton();
		shopexam.loginWithWrongEmail();

		String myaccountcheck = shopexam.invalidErrorMessage();
		assertEquals("Error message is not correct", "There is 1 error", myaccountcheck);
		String emailcheck = shopexam.invalidEmailMessagealert();
		assertEquals("Error message for invalid email address is not correct ", "Invalid email address.", emailcheck);

		driver.quit();
	}

	@Test
	public void loginWithWrongPassword() throws InterruptedException {
		loadShopPage();
		shopexam.pressSignInButton();
		shopexam.loginWithWrongPassword();

		String myaccountcheck = shopexam.invalidErrorMessage();
		assertEquals("Error message is not correct", "There is 1 error", myaccountcheck);
		String errorcheck = shopexam.invalidEmailMessagealert();
		assertEquals("Error message for invalid password is not correct ", "Invalid password.", errorcheck);
	
		driver.quit();
	}

	@Test
	public void createAccountWithoutFirstName() throws InterruptedException {
		loadShopPage();
		shopexam.pressSignInButton();
		shopexam.sendEmail();
		shopexam.createNewaccountwithoutFirstName("Dilovski", "Adfweg1234", "Sofia,Bulgaria", "Sofia", "18035",
				"0893426543", "SF");

		String errorcheck = shopexam.invalidErrorMessageCount();
		assertEquals("Error message is not correct", "There is 1 error", errorcheck);
		String wrongemail = shopexam.invalidEmailMessage();
		assertEquals("Error message for invalid firstname is not correct", "firstname", wrongemail);

		driver.quit();

	}

	@Test
	public void createAccountWithoutFirstNameAndLastName() throws InterruptedException {
		loadShopPage();
		shopexam.pressSignInButton();
		shopexam.sendEmail();
		shopexam.createNewaccountwithoutFirstNameAndLastName("Adfweg1234", "Sofia,Bulgaria", "Sofia", "18035",
				"0893426543", "SF");

		String errorcheck = shopexam.invalidErrorMessageCount();
		assertEquals("Error message is not correct ", "There are 2 errors", errorcheck);

		String wrongemail = shopexam.invalidEmailMessage();
		assertEquals("Error message for lastname is not correct", "lastname", wrongemail);

		String wrongepassword = shopexam.firstNameisRequiredMessage();
		assertEquals("Error message for firstname is not correct", "firstname", wrongepassword);

		driver.quit();

	}

}
