package net.progress.selenium.getting_started;

import java.util.Random;
import java.util.Set;

import org.junit.Test;
import org.openqa.selenium.*;

import pageObjects.LoginPage;
import pageObjects.LoginPageModel;
import utils.Common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LogIn {
    WebDriver driver;
    LoginPage loginPagePF;

    public LogIn(){
        driver=Common.getDriver();
        loginPagePF = new LoginPage(this.driver);
    }

    @Test
    public void loginPositive() throws InterruptedException {
        WebDriver driver = Common.getDriver();
        String username = generateEmail();

        driver.get("http://practice.automationtesting.in/my-account/");
        driver.findElement(By.id("reg_email")).sendKeys(username + "@gmail.com");
        driver.findElement(By.id("reg_password")).sendKeys("Autom12!@#$");
        driver.findElement(By.cssSelector("#customer_login > div.u-column2.col-2 > "
                + "form > p.woocomerce-FormRow.form-row > " +
                "input.woocommerce-Button.button")).click();

        String cssSelectorHello = "#page-36 > div > div.woocommerce > " +
                "div > p:nth-child(1) > strong";
        assertEquals(String.format("There is no element with CSS selector %s",
                cssSelectorHello), 1,
                driver.findElements(By.cssSelector(cssSelectorHello)).size());
        assertEquals("Username is not the expected one", username,
                driver.findElement(By.cssSelector(cssSelectorHello)).getText());

        driver.quit();
    }

    @Test
    public void registerPositivePOM(){
        LoginPageModel loginPage=new LoginPageModel();

        loginPage.navigateToPage();
        loginPage.register("Qwemnb789632145");

        assertTrue("Login is not successful",
                loginPage.isRedirectSuccessful());
    }

    @Test
    public void loginPositivePageFactory(){
        loginPagePF.load();
        loginPagePF.isLoaded();
        String username="";
        String password="";

        loginPagePF.register(username, password);

//        loginPagePF.login(username, password);
        loginPagePF.isLoginSuccessful();

        assertTrue("Login is not successful",loginPagePF.isLoginSuccessful());
    }

    @Test
    public void upoladImage(){
        WebDriver driver = Common.getDriver();

        driver.get("http://demo.automationtesting.in/Register.html");
        driver.findElement(By.id("imagesrc")).sendKeys("C:\\Снимки\\IMG_20190410_102723.jpg");
        System.out.println(driver.findElement(By.id("imagesrc")).getText());
    }



    @Test
    public void checkFrames() {
        WebDriver driver = Common.getDriver();
        driver.get("https://www.toolsqa.com/iframe-practice-page/");
        driver.switchTo().frame("IF1");
        driver.findElement(By.cssSelector("#primary-menu > li.menu-item.menu-item-type-custom.menu-item-object-custom." +
                "menu-item-has-children.menu-item-27328.has-children > " +
                "ul > li.menu-item.menu-item-type-post_type.menu-item-object-page.menu-item-28066 > a > span > span")).click();

        driver.switchTo().defaultContent();
        driver.findElement(By.cssSelector("#primary-menu > li.menu-item.menu-item-type-custom." +
                "menu-item-object-custom.menu-item-34232 > a > span > span")).click();


        driver.switchTo().alert();
    }

    @Test
    public void windowsSwitch() throws InterruptedException {
        WebDriver driver = Common.getDriver();
        driver.get("http://toolsqa.com/automation-practice-switch-windows/");
        String currentHandle = driver.getWindowHandle();

        driver.findElement(By.cssSelector("#content > div.vc_row.wpb_row.vc_row-fluid.dt-default > div.wpb_column.vc_column_container" +
                ".vc_col-sm-8 > div > div > div > div > p:nth-child(6) > button")).click();

        Set<String> handles = driver.getWindowHandles();
        for (String handle : handles) {
            if (!handle.equals(currentHandle)) {
                driver.switchTo().window(handle);
            }
        }

        System.out.println(driver.findElement(By.cssSelector("body")).getText());
        driver.close();
        driver.switchTo().window(currentHandle);
        driver.findElement(By.id("button1")).click();
    }

    @Test
    public void alertAndTimeout() throws InterruptedException {
        WebDriver driver = Common.getDriver();
        driver.get("http://toolsqa.com/automation-practice-switch-windows/");

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,400)");

        WebElement element = driver.findElement(By.id("timingAlert"));
        js.executeScript("arguments[0].scrollIntoView(true);", element);
        //Thread.sleep(500);
        driver.findElement(By.id("timingAlert")).click();
//        WebDriverWait wait=new WebDriverWait(driver, 10);.until(ExpectedConditions.elementToBeClickable(By.id("timingAlert")));
//        Boolean button=wait.until(ExpectedConditions.presenceOfElementLocated()
//        driver.findElement(By.id("timingAlert")).click();
//
//
//        Alert alert=(new WebDriverWait(driver,10)).until(ExpectedConditions.alertIsPresent());
//
//        alert.accept();
    }


    public void registerWithInvalidEmail() {
        WebDriver driver = Common.getDriver();
        driver.get("http://practice.automationtesting.in/my-account/");
        driver.findElement(By.id("reg_email")).sendKeys("registerWithInvalidEmail@email");
        driver.findElement(By.id("reg_password")).sendKeys("MyPassword12!@#$*");
        driver.findElement(By.cssSelector("#customer_login > div.u-column2.col-2 > "
                + "form > p.woocomerce-FormRow.form-row > "
                + "input.woocommerce-Button.button")).click();

        String errorMessageText = driver.findElement(By.className("woocommerce-error")).getText();
        System.out.println(errorMessageText);
        assertEquals(errorMessageText, "Error: Please provide a valid email address.");
    }


    public void logInWithIncorrectUsernameAndPassword() throws InterruptedException {
        WebDriver driver = Common.getDriver();
        driver.get("http://practice.automationtesting.in/my-account/");
        driver.findElement(By.id("username")).sendKeys("notValidUsername");
        driver.findElement(By.id("password")).sendKeys("wrongPass");
        driver.findElement(By.cssSelector("#customer_login > div.u-column1.col-1 > "
                + "form > p:nth-child(3) > input.woocommerce-Button.button")).click();

//			String notValidUsername=": Invalid username. ";
//			Assert.assertEquals(String.format("There is no element with CSS selector %s", notValidUsername), 
//					1, driver.findElements(By.cssSelector(notValidUsername)).size());
//			Assert.assertEquals(": Invalid username. ", notValidUsername,
//					driver.findElement(By.cssSelector(notValidUsername)).getText());
//			

        String errorMessageText = driver.findElement(By.className("woocommerce-error")).getText();
        System.out.println(errorMessageText);
        assertEquals(errorMessageText, "ERROR: Invalid username. Lost your password?");
    }


    public void rememberMeCheck() throws InterruptedException {
        WebDriver driver = Common.getDriver();
        driver.get("http://practice.automationtesting.in/my-account/");

        driver.findElement(By.id("username")).sendKeys("mariarantonova@gmail.com");
        driver.findElement(By.id("password")).sendKeys("Autom12!@#$");
        driver.findElement(By.id("rememberme")).click();
        driver.findElement(By.name("login")).click();
    }


    private String generateEmail() {
        String result = "";

        Random random = new Random();
        result = "newuser" + random.nextInt();

        return result;
    }
}
