package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Common;
import java.util.Random;


public class LoginPageModel {
    private WebDriver driver;

    private WebElement username;
    private WebElement password;
    private WebElement registerButton;

    private WebDriverWait wait;
    private String usernameString;

    public LoginPageModel() {
        driver = Common.getDriver();
        wait = new WebDriverWait(driver, 10);
    }

    public void register(String password) {
        String username=generateEmail();
        this.username.sendKeys(username+"@gmail.com");
        this.password.sendKeys(password);
        wait.until(ExpectedConditions.elementToBeClickable(registerButton)).click();
        usernameString=username;
    }

    public void navigateToPage() {
        driver.get("http://practice.automationtesting.in/my-account/");
        intiElements();
    }

    private void intiElements(){
        username = driver.findElement(By.id("reg_email"));
        password = driver.findElement(By.id("reg_password"));
        registerButton = driver.findElement(By.name("register"));
    }

    public boolean isRedirectSuccessful() {
        boolean result = true;

        String cssSelectorHello = "#page-36 > div > div.woocommerce > " +
                "div > p:nth-child(1) > strong";
        if (driver.findElements(By.cssSelector(cssSelectorHello)).size() == 0) {
            result = false;
        } else {
            if (!driver.findElement(By.cssSelector(cssSelectorHello)).getText().equals(usernameString)) {
                result = false;
            }
        }

        return result;
    }

    private String generateEmail() {
        String result = "";

        Random random = new Random();
        result = "newuser" + random.nextInt();

        return result;
    }
}
