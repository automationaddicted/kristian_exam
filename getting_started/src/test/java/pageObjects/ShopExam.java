package pageObjects;

import static org.junit.Assert.assertEquals;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ShopExam extends LoadableComponent<ShopExam> {
	private WebDriver driver;
	WebDriverWait wait;
	String password = "Afg2345fgehgh";

	@FindBy(how = How.CSS, using = "#header > div.nav > div > div > nav > div.header_user_info > a")
	private WebElement signinbutton;

	@FindBy(how = How.ID, using = "email_create")
	private WebElement emailaddressfield;

	@FindBy(how = How.CSS, using = "#create-account_form > h3")
	private WebElement createanaccounttitle;

	@FindBy(how = How.CSS, using = "#SubmitCreate > span")
	private WebElement createaccountbutton;

	@FindBy(how = How.CSS, using = "#account-creation_form > div:nth-child(1) > h3")
	private WebElement personalinformationtitle;

	@FindBy(how = How.ID, using = "id_gender1")
	private WebElement mrradiobutton;

	@FindBy(how = How.ID, using = "customer_firstname")
	private WebElement firstnamefield;

	@FindBy(how = How.ID, using = "customer_lastname")
	private WebElement lastnamefield;

	@FindBy(how = How.ID, using = "passwd")
	private WebElement passwordfield;

	@FindBy(how = How.ID, using = "address1")
	private WebElement addressfield;

	@FindBy(how = How.ID, using = "city")
	private WebElement cityfield;

	@FindBy(how = How.CSS, using = "#id_state > option:nth-child(8)")
	private WebElement connecticutfieldoption;

	@FindBy(how = How.ID, using = "postcode")
	private WebElement postcodefield;

	@FindBy(how = How.ID, using = "id_country")
	private WebElement countryfield;

	@FindBy(how = How.CSS, using = "#id_country > option:nth-child(2)")
	private WebElement usaoption;

	@FindBy(how = How.ID, using = "phone_mobile")
	private WebElement phonemobilefield;

	@FindBy(how = How.ID, using = "alias")
	private WebElement addressalias;

	@FindBy(how = How.ID, using = "submitAccount")
	private WebElement registerbutton;

	@FindBy(how = How.CSS, using = "#center_column > h1")
	private WebElement Myaccounttitle;

	@FindBy(how = How.CSS, using = "#header > div.nav > div > div > nav > div:nth-child(2) > a")
	private WebElement signoutbutton;

	@FindBy(how = How.ID, using = "email")
	private WebElement emailaddressfieldlogin;

	@FindBy(how = How.ID, using = "passwd")
	private WebElement passwordloginfield;

	@FindBy(how = How.CSS, using = "#SubmitLogin > span\r\n")
	private WebElement signinbuttonlogin;

	@FindBy(how = How.CSS, using = "#center_column > div.alert.alert-danger > p")
	private WebElement alertmessageerrorcount;

	@FindBy(how = How.CSS, using = "#center_column > div.alert.alert-danger > ol > li")
	private WebElement alertmessagewrongemailandpassword;

	@FindBy(how = How.CSS, using = "#create_account_error > ol > li")
	private WebElement wrongemailmessage;

	@FindBy(how = How.CSS, using = "#center_column > div > p")
	private WebElement erormessagecount;

	@FindBy(how = How.CSS, using = "#center_column > div > ol > li > b")
	private WebElement firstnameerormessage;

	@FindBy(how = How.CSS, using = "#center_column > div > ol > li:nth-child(1)")
	private WebElement lastnameerormessage;

	@FindBy(how = How.CSS, using = "#center_column > div > ol > li:nth-child(2) > b")
	private WebElement firstnameerormessageisrequired;

	public ShopExam(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, 10);
	}

	@Override
	public void load() {
		driver.get(" http://automationpractice.com/index.php");

	}

	@Override
	public void isLoaded() throws Error {
		assertEquals("Page is not loaded", "http://automationpractice.com/index.php", driver.getCurrentUrl());
	}

	public void pressSignInButton() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(signinbutton)).click();

	}

	public String isRedirectSuccessfulToPersonalInfo() throws InterruptedException {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.cssSelector("#account-creation_form > div:nth-child(1) > h3"))));
		return this.personalinformationtitle.getText();
	}

	public String isRedirectSuccessfulToCreateanAccount() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated((By.cssSelector("#create-account_form > h3"))));
		return this.createanaccounttitle.getText();
	}

	public void sendInvalidEmail() throws InterruptedException {
		String username = generateEmail();
		pressSignInButton();
		this.emailaddressfield.sendKeys(username);
		this.createaccountbutton.click();
	}

	public void createNewaccountonlyMandatoryFields(String firstname, String lastname, String password, String address,
			String city, String postcode, String mobilenumber, String alias) throws InterruptedException {
		
		wait.until(ExpectedConditions.elementToBeClickable(mrradiobutton)).click();
		this.firstnamefield.sendKeys(firstname);
		this.lastnamefield.sendKeys(lastname);
		this.passwordfield.sendKeys(password);
		this.addressfield.sendKeys(address);
		this.cityfield.sendKeys(city);
		wait.until(ExpectedConditions.elementToBeClickable(connecticutfieldoption)).click();
		this.postcodefield.sendKeys(postcode);
		wait.until(ExpectedConditions.elementToBeClickable(usaoption)).click();
		this.phonemobilefield.sendKeys(mobilenumber);
		this.addressalias.clear();
		this.addressalias.sendKeys(alias);
		wait.until(ExpectedConditions.elementToBeClickable(registerbutton)).click();
	}

	public void createNewaccountwithoutFirstName(String lastname, String password, String address, String city,
			String postcode, String mobilenumber, String alias) throws InterruptedException {
		
		wait.until(ExpectedConditions.elementToBeClickable(mrradiobutton)).click();
		this.lastnamefield.sendKeys(lastname);
		this.passwordfield.sendKeys(password);
		this.addressfield.sendKeys(address);
		this.cityfield.sendKeys(city);
		wait.until(ExpectedConditions.elementToBeClickable(connecticutfieldoption)).click();
		this.postcodefield.sendKeys(postcode);
		wait.until(ExpectedConditions.elementToBeClickable(usaoption)).click();
		this.phonemobilefield.sendKeys(mobilenumber);
		this.addressalias.clear();
		this.addressalias.sendKeys(alias);
		wait.until(ExpectedConditions.elementToBeClickable(registerbutton)).click();
	}

	public void createNewaccountwithoutFirstNameAndLastName(String password, String address, String city,
			String postcode, String mobilenumber, String alias) throws InterruptedException {
		
		wait.until(ExpectedConditions.elementToBeClickable(mrradiobutton)).click();

		this.passwordfield.sendKeys(password);
		this.addressfield.sendKeys(address);
		this.cityfield.sendKeys(city);
		wait.until(ExpectedConditions.elementToBeClickable(connecticutfieldoption)).click();
		this.postcodefield.sendKeys(postcode);
		wait.until(ExpectedConditions.elementToBeClickable(usaoption)).click();
		this.phonemobilefield.sendKeys(mobilenumber);
		this.addressalias.clear();
		this.addressalias.sendKeys(alias);
		wait.until(ExpectedConditions.elementToBeClickable(registerbutton)).click();
	}

	public String createNewaccount() throws InterruptedException {
		
		String newUser = generateEmail() + "@gmail.com";

		this.emailaddressfield.sendKeys(newUser);
		wait.until(ExpectedConditions.elementToBeClickable(createaccountbutton)).click();
		wait.until(ExpectedConditions.elementToBeClickable(mrradiobutton)).click();
		this.firstnamefield.sendKeys("Kristian");
		this.lastnamefield.sendKeys("Dilovski");
		this.passwordfield.sendKeys(password);
		this.addressfield.sendKeys("Sofia,Bulgaria");
		this.cityfield.sendKeys("Sofia");
		wait.until(ExpectedConditions.elementToBeClickable(connecticutfieldoption)).click();
		this.postcodefield.sendKeys("24542");
		wait.until(ExpectedConditions.elementToBeClickable(usaoption)).click();
		this.phonemobilefield.sendKeys("0875245672");
		this.addressalias.clear();
		this.addressalias.sendKeys("MF");
		wait.until(ExpectedConditions.elementToBeClickable(registerbutton)).click();
		wait.until(ExpectedConditions.elementToBeClickable(signoutbutton)).click();

		return newUser;

	}

	public void pressSignoutButton() {
		wait.until(ExpectedConditions.elementToBeClickable(signoutbutton)).click();
	}

	public void loginPositive() throws InterruptedException {
		String newUSer = createNewaccount();

		this.emailaddressfieldlogin.sendKeys(newUSer);
		this.passwordloginfield.sendKeys(password);
		wait.until(ExpectedConditions.elementToBeClickable(signinbuttonlogin)).click();

	}

	public void loginWithWrongEmail() throws InterruptedException {
		String newUSer = createNewaccount();

		this.emailaddressfieldlogin.sendKeys("sdfgfgeg");
		this.passwordloginfield.sendKeys(password);
		wait.until(ExpectedConditions.elementToBeClickable(signinbuttonlogin)).click();
	}

	public void loginWithWrongPassword() throws InterruptedException {
		String newUSer = createNewaccount();

		this.emailaddressfieldlogin.sendKeys(newUSer);
		this.passwordloginfield.sendKeys("124");
		wait.until(ExpectedConditions.elementToBeClickable(signinbuttonlogin)).click();
	}

	public String checkErrorMessageEmail() {
		wait.until(ExpectedConditions.visibilityOfElementLocated((By.cssSelector("#create_account_error > ol > li"))));
		return this.wrongemailmessage.getText();

	}

	public void sendEmail() {
		String username = generateEmail();

		this.emailaddressfield.sendKeys(username + "@gmail.com");
		wait.until(ExpectedConditions.elementToBeClickable(createaccountbutton)).click();
	}

	public String checkMyaccountPagetitle() {
		return Myaccounttitle.getText();
	}

	public String invalidErrorMessage() {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.cssSelector("#center_column > div.alert.alert-danger > p"))));
		return alertmessageerrorcount.getText();
	}

	public String invalidEmailMessagealert() {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.cssSelector("#center_column > div.alert.alert-danger > ol > li"))));
		return alertmessagewrongemailandpassword.getText();
	}


	public String invalidErrorMessageCount() {
		wait.until(ExpectedConditions.visibilityOfElementLocated((By.cssSelector("#center_column > div > p"))));
		return erormessagecount.getText();
	}

	public String invalidEmailMessage() {
		wait.until(
				ExpectedConditions.visibilityOfElementLocated((By.cssSelector("#center_column > div > ol > li > b"))));
		return firstnameerormessage.getText();
	}

	public String lastNameisRequiredMessage() {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.cssSelector("#center_column > div > ol > li:nth-child(1) > b"))));
		return lastnameerormessage.getText();
	}

	public String firstNameisRequiredMessage() {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated((By.cssSelector("#center_column > div > ol > li:nth-child(2) > b"))));
		return firstnameerormessageisrequired.getText();
	}

	private String generateEmail() {
		String result = "";
		Random random = new Random();
		result = "user" + random.nextInt();

		return result;
	}
}
