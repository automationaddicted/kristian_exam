package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class LoginPage extends LoadableComponent<LoginPage> {
    private WebDriver driver;
    WebDriverWait wait;
    String usernameString;

    @FindBy(how = How.ID, using = "reg_email")
    private WebElement userName;

    @FindBy(how = How.ID, using = "reg_password")
    private WebElement  password;

    @FindBy(how = How.NAME, using = "register")
    private WebElement registerButton;

    @FindBy(how = How.ID, using = "username")
    private WebElement loginName;

    @FindBy(how = How.ID, using = "password")
    private WebElement loginPassword;

    @FindBy(how = How.NAME, using = "login")
    private WebElement loginButton;


    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 10);
    }

    @Override
    public void load() {
        driver.get("http://practice.automationtesting.in/my-account/");
    }

    @Override
    public void isLoaded() throws Error {
        assertEquals("Page is not loaded", "http://practice.automationtesting.in/my-account/",
                driver.getCurrentUrl());
    }

    public void register(String userName, String password) {
        this.userName.sendKeys(userName);
        
        registerButton.click();
    }

    public void login(String username, String password) throws InterruptedException {
        this.loginName.sendKeys(username);
        this.loginPassword.sendKeys(password);
        wait.until(ExpectedConditions.elementToBeClickable(loginButton)).click();
    }
    
    public void registerwithoutemail(String password)  {
    	this.password.sendKeys(password);
        registerButton.click();
    	
    }

    public boolean isLoginSuccessful() {
        return isRedirectSuccessful();
    }

    public boolean isRedirectSuccessful() {
        boolean result = true;

        String cssSelectorHello = "#page-36 > div > div.woocommerce > " +
                "div > p:nth-child(1) > strong";
        if (driver.findElements(By.cssSelector(cssSelectorHello)).size() == 0) {
            result = false;
        } else {
            if (!driver.findElement(By.cssSelector(cssSelectorHello)).getText().equals(usernameString)) {
                result = false;
            }
        }

        return result;
    }
}
