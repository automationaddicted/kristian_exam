package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.concurrent.TimeUnit;

public class Common {
	public static WebDriver getDriver() {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities = DesiredCapabilities.firefox();
		capabilities.setCapability("marionette", false);
		FirefoxOptions options=new FirefoxOptions(capabilities);
		WebDriver driver=new FirefoxDriver(options);
		driver.manage().window().maximize();
//		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		return driver;
	}
}